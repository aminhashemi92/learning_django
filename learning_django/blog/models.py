from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.utils.html import format_html
from django.core.validators import MinLengthValidator, MaxLengthValidator, RegexValidator
from django.core.exceptions import ValidationError

from django.contrib.contenttypes.fields import GenericRelation
from comment.models import Comment
from django.urls import reverse

# Create your models here.
class Article(models.Model):
    CONDITION_CHOICE = (
        ('p', 'published'),
        ('d', 'draft'),
        ('r', 'rejected')
    )

    title = models.CharField(max_length=200)
    content = models.TextField()

    id_number = models.CharField(
        max_length=11,
        blank=True,
        validators=[
            MinLengthValidator(10),
            MaxLengthValidator(11),
            RegexValidator(
                regex=r'^[0-9]*$',
                message='نام کاربری فقط شامل عدد می تواند باشد',
                code='nomatch',
            )
        ],
        verbose_name='شماره‌ملی/شناسه‌ملی',
    )

    thumbnail =  models.ImageField(upload_to='article_image', blank=True)

    author = models.ForeignKey(get_user_model(), on_delete=models.SET_NULL, null=True)

    condition = models.CharField(max_length=1, choices=CONDITION_CHOICE, blank=True, default='d')

    slug = models.SlugField(max_length =200, unique=True, blank=True, null=True)

    status = models.BooleanField(default=True)

    publish_date = models.DateTimeField(auto_now_add=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    comments = GenericRelation(Comment)

    def get_pic(self):
        a = '<img width=80 src='+self.thumbnail.url+'>'
        return format_html(a)
    
    def mstatus(self):
        if self.status == True:
            a = "<img src='/static/admin/img/icon-yes.svg' alt='True'>"
        else:
            a = "<img src='/static/admin/img/icon-no.svg' alt='False'>"
        return format_html(a)
    
    def __str__(self):
        return str(self.title)
    
    def get_absolute_url(self):
        return reverse('post_detail_url', kwargs={'slug': self.slug})


