from django.urls import path
from .views import *

app_name = "blog"

urlpatterns = [
    path('', blog, name='blog'),
    path('profile', profile, name='profile'),
    path('ajax_more', ajax_more, name='ajax_more'),
    path('add_article', add_article, name='add_article'),
    path('article_details/<int:pk>', article_details, name='article_details'),
]