# Generated by Django 4.2.6 on 2023-11-25 15:50

import django.core.validators
from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='id_number',
            field=models.CharField(default=django.utils.timezone.now, max_length=11, unique=True, validators=[django.core.validators.MinLengthValidator(10), django.core.validators.MaxLengthValidator(11), django.core.validators.RegexValidator(code='nomatch', message='نام کاربری فقط شامل عدد می تواند باشد', regex='^[0-9]*$')], verbose_name='شماره\u200cملی/شناسه\u200cملی'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='article',
            name='condition',
            field=models.CharField(blank=True, choices=[('p', 'published'), ('d', 'draft'), ('r', 'rejected')], default='d', max_length=1),
        ),
        migrations.AlterField(
            model_name='article',
            name='publish_date',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='thumbnail',
            field=models.ImageField(blank=True, upload_to='article_image'),
        ),
    ]
