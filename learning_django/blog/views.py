from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse, HttpResponseNotFound
from django.contrib.auth import get_user_model
from .models import Article
from .forms import ArticleForm

# Create your views here.
def blog(request):
    user = request.user
    # articles = Article.objects.filter(status=True, condition='p')
    articles = Article.objects.all()
    context={
        'articles':articles,
    }
    return render(request, 'blog/blog.html', context)


def profile(request):
    return HttpResponse("hi")


def add_article(request):
    form = ArticleForm()

    if request.POST:
        form = ArticleForm(request.POST, request.FILES)
        if form.is_valid():
            obj = form.save()
            obj.author = request.user
            obj.save()
            return redirect('blog:blog')


    context = {
        'form' : form ,
        
    }
    return render(request, 'blog/add_article.html', context)


def article_details(request, pk):

    try:
        article = Article.objects.get(id=pk)
    except Article.DoesNotExist:
        return HttpResponse("NOT FOUND")


    context = {
        'article':article,
    }
    return render(request, 'blog/article_details.html', context)



def ajax_more(request):
    article_id = request.GET['articleId']
    article = Article.objects.get(id=article_id)
    context = {
        'article':article,
    }
    return render(request, 'blog/ajax_more.html', context)