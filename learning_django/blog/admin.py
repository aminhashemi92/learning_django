from django.contrib import admin
from .models import Article
# Register your models here.

class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'status', 'condition', 'publish_date', 'created')
    list_filter = ([ 'author', 'status'])
    search_fields = ('title', 'content')
    # prepopulated_fields = {'slug':('title',)}
    # actions = [make_active, make_diactive]
    # actions = [make_active, make_diactive]

admin.site.register(Article, ArticleAdmin)
